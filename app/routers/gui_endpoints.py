'''API endpoints related to content management'''
import os
import requests
from fastapi import APIRouter, Request, Path
from fastapi.templating import Jinja2Templates
from pydantic import constr

from dependencies import log

VACHAN_DOMAIN = os.environ.get("VACHAN_DOMAIN", "api.vachanengine.org")

router = APIRouter()

templates = Jinja2Templates(directory="templates")

@router.get("/v2/demos/")
def get_all_urls(request: Request):
    '''Lists all the demos available in an HTML page'''
    url_list = [{"path": route.path, "name": route.name} for route in router.routes]
    links_html = ""
    for item in url_list:
        if "bible-verses" in item['path']:
            item['path'] = item['path'].split("bible-verses")[0]+\
                            "bible-verses/en_ERV_x_bible/jhn/3/14-17"
        one_link = f"<a href={item['path']}>{item['name']}</a>"
        links_html += one_link
    return templates.TemplateResponse("index.html",
                {"request":request,
                "links":links_html,
                "BASE_URL":f"https://{VACHAN_DOMAIN}"})

@router.get('/v2/demos/user/login', status_code=200,
    tags=["User"])
async def user_login(request: Request):
    '''Returns a rendered HTML page with login form allowing users to generate a token'''
    log.info('In router function user_login')
    return templates.TemplateResponse("login.html",
                {"request": request,
                "BASE_URL":f"https://{VACHAN_DOMAIN}"})

@router.get('/v2/demos/user/register', status_code=200,
    tags=["User"])
async def user_registration(request: Request):
    '''Returns a rendered HTML page with registration form allowing users to create an account'''
    log.info('In router function user_registration')
    return templates.TemplateResponse("register.html",
                {"request": request,
                "BASE_URL":f"https://{VACHAN_DOMAIN}"})


@router.get('/v2/demos/nlp/alignment', status_code=200,
    tags=["User"])
async def alignment_viewer(request: Request):
    '''Returns a rendered HTML which takes alignment data as per our format and validate it'''
    log.info('In router function alignment_viewer')
    return templates.TemplateResponse("alignment.html", {
                "request": request,
                "BASE_URL":f"https://{VACHAN_DOMAIN}"
                })


@router.get('/v2/demos/usfm-grammar', status_code=200,
    tags=["User"])
async def interface_to_usfm_grammar(request: Request):
    '''Returns a rendered HTML page to upload/edit USFMs and convert to supported formats'''
    log.info('In router function interface_to_usfm_grammar')
    return templates.TemplateResponse("usfm-grammar.html", {
                "request": request,
                "BASE_URL":f"https://{VACHAN_DOMAIN}"
                })

source_name_pattern = constr(regex=r'^[a-zA-Z]+(-[a-zA-Z0-9]+)*_[A-Z]+_[\w\.]+_[a-z]+$')
book_code_pattern = constr(regex=r'^[a-zA-Z1-9][a-zA-Z][a-zA-Z]$')
verses_pattern = constr(regex=r'\d+(-\d+)?')

@router.get('/v2/demos/bible-verses/{bible}/{book}/{chapter}/{verses}', status_code=200,
    tags=["User"])
async def fetch_bible_verses(request: Request,
        bible: source_name_pattern = Path(..., example="en_ERV_x_bible"),
        book: book_code_pattern = Path(..., example="MAT"),
        chapter: int = Path(..., example="24"),
        verses: verses_pattern = Path(..., example="14") ):
    '''Returns a rendered HTML page to upload/edit USFMs and convert to supported formats'''
    log.info('In router function fetch_bible_verses')
    verses = verses.split("-")
    verse = verses[0]
    last_verse = verses[-1]
    response = requests.get("https://stagingapi.vachanengine.org/v2/bibles/"+
        f"{bible}/verses?book_code={book}&chapter={chapter}&"+
        f"verse={verse}&last_verse={last_verse}&active=true&skip=0&limit=100", timeout=60)
    verses_n_texts = ""
    if response.status_code == 200:
        for item in response.json():
            verse_str = f"<p> <sup>{item['reference']['verseNumber']}</sup> {item['verseText']}</p>"
            verses_n_texts += verse_str
    else: 
        error_resp = response.json()
        error_str = f"<p class=\"text-danger\">{error_resp['error']: {error_resp['details']}}</p>"
        verses_n_texts = error_str
    return templates.TemplateResponse("bible-verses.html",
        {"request": request, 
        "book_chap": f"{book.upper()}: {chapter}",
        "verses_n_texts": verses_n_texts}
        )

