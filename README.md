# Vachan Demos

Simple web based GUIs to demonstrate the vachan-engine features. This can be used by UI developers for reference or by management/users as prototypes to envision how the back-end features work.

Built without the help of any UI frameworks, but with basic HTML and CSS, with the help of tailwind.css. Each API endpoint essentially returns a rendered HTML page. So they are single page applications with bare minimum functionalities to just demonstarte the intented backend feature.

## Frame works and libraries
* FastAPI 
* bootstrap.css

## Install and Run for local developement
1. Clone the repo
    ```
       git clone <this repo url>
       cd vachan-demos
    ``` 
1. Set up virtual Environment
    ```
    python3 -m venv ENV
    source ENV/bin/activate
    ```
    NOTE: Can use `deactivate` command to exit the virtual environment later.
1. Install dependancies
    `pip install -r requirements.txt`

1. Environment Variables
    `VACHAN_DOMAIN="api.vachanengine.org` or other values can set in the .bashrc file in home folder or in .env files. If not set, the default value `api.vachanengine.org` will be used.

1. Start Server
    ```
    cd app
    uvicorn main:app --port 8000
    ```
1. Exit
    `ctrl+c` in the terminal where uvicorn is running.

<!-- ## Run Test -->

## Build and Run using Docker

1. `docker build .`
1. `docker run -v vachan-demos-logs:/app/logs/ -p 8000:8000 -e VACHAN_DOMAIN=stagingapi.vachanengine.org <imageid>`

Environment variable, `VACHAN_DOMAIN` has its default value is `api.vachanengine.org`.
Optionally, environment variable `VACHAN_LOGGING_LEVEL` can also be set as "INFO", "WARNING", "ERROR" etc.
The app runs the API server on the port `8000`.
